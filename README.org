* SICP Course
  [[https://mitpress.mit.edu/sicp/][SICP: Structure and Intreptation of Computer Programs]] coursework.
* [[file:self-study/index.org][Self Study]]
  [[file:self-study/index.org][Self Study]] contains some self study materials.
* [[https://www.comp.nus.edu.sg/~cs1101s/sicp/][Structure and Interpretation of Computer Programs, JavaScript Adaptation]]
* [[http://www.sicpdistilled.com][SICP Distilled]]
  [[http://www.sicpdistilled.com][SICP Distilled]] contains SICP exercises distilled into Clojure programs.
** [[https://news.ycombinator.com/item?id=17643452][Hacker News Discussion #17643452]]
#+CAPTOION: bm3719 comments
#+BEGIN_QUOTE
I regret having wasted my time trying to work through this about 2 years ago:
- There's a lot of sloppy content, i.e. misspellings that a spell checker should catch.
- Some code is syntactically incorrect. Some hasn't been ported over and is still in Scheme.
- The author's claim that the core points are present is somewhat dubious, since only parts of chapter 1 are reasonably complete.
- The difference in writing quality from the original text and what the author has changed is quite jarring. The original's prose is efficient, insightful, and deeply connected with other content. The new stuff is at best blog-quality.
- The author has certainly abandoned this project. Nor does it look like he'd be up to the task, even if he did have the motivation for it.
#+END_QUOTE
* Inspired by SICP
** [[http://www.composingprograms.com/][Composing Programs, a free online introduction to programming and computer science.]]
#+CAPTION: Introduction from the website
#+BEGIN_QUOTE
In the tradition of SICP, this text focuses on methods for abstraction, programming paradigms, and techniques for managing the complexity of large programs.
These concepts are illustrated primarily using the Python 3 programming language.
#+END_QUOTE
